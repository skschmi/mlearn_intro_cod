import csv
import sklearn
import numpy
from sklearn import datasets
from sklearn import svm
from sklearn.naive_bayes import GaussianNB



class DataStore():

    def __init__(self,target_names,data,target):
        self.target_names = target_names
        self.data = data
        self.target = target
    
    @classmethod
    def initFromCSVFile(cls,filename,category_column):

        #Import the data into a list   
        csvf = open(filename)
        reader = csv.reader(csvf)
        all_data = []
        for line in reader:
            all_data.append(line)
        
        # Convert the list to a numpy array
        np_all_data = numpy.array(all_data)
        n_lines = len(np_all_data[:,0])
    
        # Create the target_names
        target_names_set = set([])
        for item in np_all_data[:,category_column]:
            target_names_set.add(item)
        target_names = numpy.array(list(target_names_set))
    
        # data without the target names
        nrows = numpy.shape(np_all_data)[0]
        ncols = numpy.shape(np_all_data)[1]-1
        data = numpy.zeros((nrows,ncols))
        if( category_column > 0 ):
            data[:,:category_column] = np_all_data[:,:category_column].astype(float)
        if( category_column < numpy.shape(np_all_data)[1]-1 ):
            data[:,category_column:] = np_all_data[:,(category_column+1):].astype(float)
    
        target = numpy.zeros(n_lines)
        # Index of the target in "target_names" for each item
        for i in xrange(0,n_lines):
            target[i] = numpy.where( target_names == np_all_data[:,category_column][i] )[0][0]
    
        return cls(target_names,data,target)
        
    @classmethod
    def initARandomSubset(cls,datastore,fraction):
        
        count = len(datastore.target)
        new_count = int(fraction*count)
        
        old_data = list(datastore.data)
        old_target = list(datastore.target)
        new_data = []
        new_target = []
        
        for i in xrange(0,new_count):
            index = int(len(old_data)*numpy.random.random(1)[0])
            new_data.append( old_data.pop(index) )
            new_target.append( old_target.pop(index) )
            
        new_datstore = DataStore(datastore.target_names, numpy.array(new_data), numpy.array(new_target))
        return new_datstore


    def split(self,training_fraction):
        # Split the data into two groups: a training set and a test set

        count = len(self.target)

        # fraction of items to be in the training set
        # (Assume "training_fraction" is 0.8 or something like that)
        training_count = int(training_fraction*count)

        test_data = list(self.data)
        test_target = list(self.target)
        training_data = []
        training_target = []

        for i in xrange(0,training_count):
            index = int(len(test_data)*numpy.random.random(1)[0])
            training_data.append( test_data.pop(index) )
            training_target.append( test_target.pop(index) )

        training_datstore = DataStore(self.target_names, numpy.array(training_data), numpy.array(training_target))
        test_datstore = DataStore(self.target_names, numpy.array(test_data), numpy.array(test_target))

        return training_datstore, test_datstore


        
print "Bias-Variance Decomposition Assignment - CS 676 Data Mining"
print "January, 2016"
print "Steven Schmidt"
print "Using COD to examine the Bias-Variance of a machine learning algorithm"
print ""
print "For each file-algorithm pair, the data is split into two parts: 80% is"
print "  used as training data, and 20% is used as testing data.  Each pair then has 10 iterations."
print "  For each iteration, a 50% subset of the training data is randomly selected and used"
print "  as the actual training data, and a result is obtained.  This is done twice, so there are two"
print "  predictions from two randomly selected traning data sets.  The COD is then computed, defined as"
print "  P(h1 != h2), where h1 is the first predicted target and h2 is the second predicted target."
print "  At the end, the average of the COD for each file-algorithm pair is computed and displayed."
print ""
print "Interpretation: The higher the COD value, the more sensitive to the data the algorithm is (for this data)"
print "                and the higher the variance.  If the COD is really low, then the bias is higher. "
print ""
print "Algorithm: Gaussian Naive Bayes"
print ""
print "Data set 1: \"arrhythmia_modified.data.txt\", contains medical data. The learning"
print "            algorithm is attempting to differentiate male from female records. There"
print "            were some missing values, so I replaced the \"?\" with a \"0\".  This may have"
print "            an impact on the accuracy, but for this assignment it seemed okay."
print ""
print "Data set 2: \"letter-recognition.data.txt\", contains data that describe images of letters"
print "            of the alphabet."
print ""
        
n_tests = 10

filenames = ["arrhythmia_modified.data.txt","letter-recognition.data.txt"]
target_col = [1,0]
cod_data = [numpy.zeros(n_tests),numpy.zeros(n_tests)]

#"support vector machine", "support vector classifier"
#"Gaussian Naive Bayes"
alg_names = [ "GaussianNB" ]
algorithms = [ GaussianNB() ]

training_frac = 0.8

for k in xrange(0,len(filenames)):

    fname = filenames[k]
    print ""
    print "==========================================================="
    print "Results for file: ",fname
    print "training/test split: ",training_frac

    #Load the data from the file
    datset = DataStore.initFromCSVFile(fname,target_col[k])
    
    #split the data into training and testing set
    training_datset, test_datset = datset.split(training_frac)

    for j in xrange(0,len(algorithms)):

        print "**********************"
        print "Algorithm: ",alg_names[j]

        clfalg = algorithms[j]

        for i in xrange(0,n_tests):

            # Randomly grab a subset of the training data
            train_subset_datset = DataStore.initARandomSubset(training_datset,0.5)
            # Fit using this data.
            clfalg.fit(train_subset_datset.data, train_subset_datset.target)
            # Predict using the fitted model.
            prediction1 = clfalg.predict(test_datset.data)
            
            
            # Randomly grab a subset of the training data
            train_subset_datset = DataStore.initARandomSubset(training_datset,0.5)
            # Fit using this data.
            clfalg.fit(train_subset_datset.data, train_subset_datset.target)
            # Predict using the fitted model.
            prediction2 = clfalg.predict(test_datset.data)

            print "-----"
            print i
            #print "h1: ",prediction1
            #print "h2: ",prediction2
            #print "h : ",test_datset.target
            cod_data[k][i] = float(len(numpy.where(prediction1 != prediction2)[0]))/float(len(test_datset.data))
            print "COD: ",len(numpy.where(prediction1 != prediction2)[0]),"/",len(test_datset.data), ", or: ",cod_data[k][i]
            print "-----"

        print alg_names[j], " Average COD: ",float(numpy.sum(cod_data[k]))/float(len(cod_data[k]))
        
    print ""
    print ""



